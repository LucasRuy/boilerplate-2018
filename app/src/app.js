// =========================================
// Import Functions  -----------------------

  // Polyfill ------------------------------
  import 'whatwg-fetch';
  import 'promise/polyfill';
  import 'nodelist-foreach-polyfill';

  // NavigationMenu ------------------------
  import DefaultFunction from './components/DefaultFunction';

// =========================================
// Init Events  ----------------------------
window.addEventListener('load', function(){
  DefaultFunction();
});
